package main

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/hamza02x/worker-demo/pool"
)

func main() {

	var wg sync.WaitGroup

	for i := 0; i < 400; i++ {
		wg.Add(1)
		go func() {
			time.Sleep(100 * time.Millisecond)
			bytes := pool.Get("http://httpbin.org/get")
			fmt.Println(string(bytes))
			wg.Done()
		}()
	}

	wg.Wait()
}
